data-docker: flask postgresql nginx admin data

marvin-docker: flask postgresql nginx manga marvin

flask:
	docker build -t python python/flask/.

postgresql:
	docker build -t database database/postgresql/.

nginx:
	docker build -t webserver webserver/nginx/.

admin:
	docker build -t login login/admin/.

manga:
	docker build -t login login/manga/.

data:
	docker build -t host host/data/.

marvin:
	docker build -t host host/marvin/.